
# Agile Lab
## Download this lab using git 

*i.e. :*
- `git clone https://gitlab.com/90cos/training/modules/agile.git`
- `git checkout -b "YourFirstNameLastInitial_Agile"`
- answer the questions
- `git commit -m "answering the Agile questions $(date")`
- `git push` 

## Agile/Scrum KSATs
- K0261	    Describe Agile Development Concept
- K0262		Describe Scrum Terms and Methodology		
- K0620		Agile Development Roles			
- K0621		Scrum Event Distinction and Process Order			
- K0622		Product Backlog Grooming			
- K0623		Daily Scrum Meeting			
- K0624		Scrum Advantages to Rapid Capability Development		
- K0627		User Story Development			
- K0889		Identify the four Agile values.	
- K0892		Identify scrum roles and responsibilities	



## Agile Lab
***Include the Sample User Stories that you created in class***  
-  
-  As an instructor I will demonstrate to others how to code so that they will successfully work on their        own
-  

  
***Answer the following questions*** 
1. What is the difference bewtween *being* Agile and *doing* Agile?
    Being agile means that a person is flexible in their work.  Doing agile means that a person is able to be flexible within a team development enviornment
2. What are the advantages of Scrum and Agile?
    Scrum and Agile allow you to identify strengths and weaknesses right away during the products life cyle and allows you to go back to the source for clarification if an issue arises or a situation changes
3. Considering your future work roles:
    - Why is being Agile important? [Be general!]
        Agile is important because it provides a work structure to make sure everyone is able to be accountable to the group
    - How will Scrum be used by you to promote success?
        Scrum will be used to promote success by using it as a measure of how achievement due to feedback on projects with team members.
4. What is the purpose of the daily scrum?
    The daily scrum is used to track progress and identify impediments early to keep a project moving forward 
5. Why is there a retrospective? 
    To identify what worked, didn't work and what can be improved upon to make future projects more successful
6. In your opinion - what is the most important Agile principle? Why?
     I believe the most important principle is "Business people and developers must work together daily throughout the project" because without cooperation and the back and forth converstations and openess to feedback the product suffers and so does the image of the team.
7. In your opinion - what is the most important Scrum value? Why?
    The most important scrum value is "opennes".  Team members have to be open and honest about their skills and situation as well as progress in order for proper decisions to be made for the progression of a project.
8. List the Scrum ceremonies
    Product backlog
    Spring planning
    daily scrum/standup
    product backlog refinement
    sprint review
    sprint retrospective
9. Why is it important to fail fast?
    To identify issues early to rectify them.
10. Place a link to a great Agile/Scrum meme here`->` 
    https://tisquirrel.files.wordpress.com/2016/01/agile-meme.jpeg