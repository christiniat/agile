
# Multiple Choice
## Some questions may have multiple correct answers

- ________is the overarching philosophy for iterative, highly transparent software development.

    - Scrum

    - Agile 

- In Agile deadlines are set at the beginning of product design.

    - True

    - False

- The ___________ runs (should run) the daily scrum meeting.

    - Product Owner

    - Scrum Master 

    - Project Owner

- The product owner assigns sprint tasks.

    - True

    - False

- The Scrum Master assigns sprint tasks.

    - True

    - False

- Story points represent projected time on task.

    - True

    - False


- What is delivered at the end of a sprint?

    - A UI

    - A document detailing the progress

    - Test cases / Unit tests for the finished sprint

    - An increment of ***done*** software


- The product backlog should be based on:

    - Scrum team choice

    - Product owner preferences

    - Size of increment

    - Value of items to be delivered


- When is the sprint retrospective performed?

    - When the product owner decides

    - When the Scrum Master decides

    - When the team decides

    - At the end of each sprint


- What should the development team do when they realize they have selected more items than they can complete in a sprint?

    - Suck it up, work overtime

    - Get more developers

    - Remove some items

    - Inform the product owner


- Who measures the team's performance?

    - The Scrum Master

    - The Product Owner

    - The dev team

- Who should contribute to the retrospective?

    - The Scrum Master

    - The Product Owner

    - The dev team


- What is done during product backlog refinement?

    - Create new tasks

    - define **done**

    - estimate backlog items

    - order product backlog items

    - brainstorm product backlog items

