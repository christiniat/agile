
# Agile Lab
## Download this lab using git 

*i.e. :*
- `git clone https://gitlab.com/90cos/training/modules/agile.git`
- `git checkout -b "YourFirstNameLastInitial_Agile"`
- answer the questions
- `git commit -m "answering the Agile questions $(date")`
- `git push` 

## Agile/Scrum KSATs
- K0261	    Describe Agile Development Concept
- K0262		Describe Scrum Terms and Methodology		
- K0620		Agile Development Roles			
- K0621		Scrum Event Distinction and Process Order			
- K0622		Product Backlog Grooming			
- K0623		Daily Scrum Meeting			
- K0624		Scrum Advantages to Rapid Capability Development		
- K0627		User Story Development			
- K0889		Identify the four Agile values.	
- K0892		Identify scrum roles and responsibilities	



## Agile Lab
***Include the Sample User Stories that you created in class***  
-  
-  
-  

  
***Answer the following questions*** 
1. What is the difference bewtween *being* Agile and *doing* Agile?
2. What are the advantages of Scrum and Agile?
3. Considering your future work roles:
    - Why is being Agile important? [Be general!]
    - How will Scrum be used by you to promote success?
4. What is the purpose of the daily scrum?
5. Why is there a retrospective? 
6. In your opinion - what is the most important Agile principle? Why?
7. In your opinion - what is the most important Scrum value? Why?
8. List the Scrum ceremonies
9. Why is it important to fail fast?
10. Place a link to a great Agile/Scrum meme here`->` 