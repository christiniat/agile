# Checkpoint
1. What is the Agile Manifesto?
2. What are the values of the Agile Manifesto?
3. What are the principles of the Agile Manifesto?
4. What are the Scrum roles?
5. List the Scrum meetings
6. What 3 things do you say / describe in the daily standup?
7. What is the main emphasis of Agile?
8. What is the relationship between Scrum and Agile?
9. Why is transparency important to Agile?
10. Why are minimum viable products (MVPs) important?
