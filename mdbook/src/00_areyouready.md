## [How to Ask Questions](http://www.catb.org/~esr/faqs/smart-questions.html)  

**Are you ready to ask a question?**  
- [x] Do you have a question?
- [ ] Did you [Google](https://lmgtfy.com/?q=how+to+google+a+question) it?
- [ ] Did you look on [Stack Overflow](https://stackoverflow.com/)?
- [ ] Did you search for a manual or API website?
- [ ] Did you try ***tinkering*** with your code? i.e.:
    - [ ] inspection 
    - [ ] experimentation 
- [ ] Is there source code?
    - [ ] Did you read the source code?
- [ ] Did you ask a knowledgable peer?



---

*Flowchart for [are you ready to ask a question?](./assets/areyoureadytoaskaquestion.png)*

***See Also:***  

* [Stack Overflow's - How to ask a good question](https://stackoverflow.com/help/how-to-ask)  

* [RTFM](http://www.catb.org/jargon/html/R/RTFM.html)  

* [STFW](https://lmgtfy.com/?q=STFW)  

For further reference:  
[Eternal September](https://en.wikipedia.org/wiki/Eternal_September)  
