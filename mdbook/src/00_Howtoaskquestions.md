## [**How to Ask Questions**](http://www.catb.org/~esr/faqs/smart-questions.html)  
---

**Are you ready to ask a question?**  

This [checklist](./00_areyouready.md) helps determine if you are ready to ask a question

---

**Where should you ask your question?**  

After you have determined that you are ready to ask a question,<br>this [checklist](./00_wheretoask.md) points to where you might ask

---

**How to ask your question**  

Having determined that you are ready to ask a question and you know the venue,<br>this [checklist](./00_howtoask.md) provides the necessary steps in articulating your question

---


**Question Scavenger Hunt**
*When asked*, begin the scavenger hunt activity [here](./Labs/QuestionScavengerHunt.md)  


***See Also:***  

* [Stack Overflow's - How to ask a good question](https://stackoverflow.com/help/how-to-ask)  

* [RTFM](http://www.catb.org/jargon/html/R/RTFM.html)  

* [STFW](https://lmgtfy.com/?q=STFW)  

For further reference:  
[Eternal September](https://en.wikipedia.org/wiki/Eternal_September)  
